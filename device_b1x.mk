$(call inherit-product, $(SRC_TARGET_DIR)/product/languages_full.mk)

# The gps config appropriate for this device
$(call inherit-product, device/common/gps/gps_us_supl.mk)

$(call inherit-product-if-exists, vendor/xolo/b1x/b1x-vendor.mk)

DEVICE_PACKAGE_OVERLAYS += device/xolo/b1x/overlay


ifeq ($(TARGET_PREBUILT_KERNEL),)
	LOCAL_KERNEL := device/xolo/b1x/prebuilt/kernel
else
	LOCAL_KERNEL := $(TARGET_PREBUILT_KERNEL)
endif

PRODUCT_COPY_FILES += \
	$(LOCAL_KERNEL):kernel \
	device/xolo/b1x/rootdir/init.recovery.mt6735.rc:root/init.recovery.mt6735.rc \
    device/xolo/b1x/recovery/root/fstab.mt6753:root/fstab.mt6735 \
    $(LOCAL_PATH)/rootdir/recovery.fstab:recovery.fstab

#PRODUCT_COPY_FILES_OVERRIDES += \
#    recovery/root/ueventd.rc \
#    root/fstab.goldfish \
#    root/init.goldfish.rc \
#    recovery/root/fstab.goldfish 



$(call inherit-product, build/target/product/full.mk)


ADDITIONAL_DEFAULT_PROPERTIES += ro.secure=0 \
ro.allow.mock.location=1 \
persist.mtk.aee.aed=on \
ro.debuggable=1 \
ro.adb.secure=0 \
persist.service.acm.enable=0 \
persist.sys.usb.config=mtp \
ro.mount.fs=EXT4 \
ro.persist.partition.support=no \
ro.cip.partition.support=no
